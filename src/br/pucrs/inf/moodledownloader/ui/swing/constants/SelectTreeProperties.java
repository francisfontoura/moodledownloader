package br.pucrs.inf.moodledownloader.ui.swing.constants;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public enum SelectTreeProperties {
    
    SOUTH_PANEL_CARD,
    BUTTON_SALVAR_ENABLED,
    PROGRESS_TEXT,
    PROGRESS;
    
}
