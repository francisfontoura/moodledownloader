package br.pucrs.inf.moodledownloader.ui.swing.constants;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public enum LoginProperties {
    
    SUBMIT_VISIBLE,
    PROGRESS_VISIBLE,
    PROGRESS_INDETERMINATE,
    PROGRESS_TEXT,
    PROGRESS,
    DISPOSED
    
}
