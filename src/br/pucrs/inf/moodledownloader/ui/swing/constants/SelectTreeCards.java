package br.pucrs.inf.moodledownloader.ui.swing.constants;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public enum SelectTreeCards {

    CONTROL, PROGRESS;

}
