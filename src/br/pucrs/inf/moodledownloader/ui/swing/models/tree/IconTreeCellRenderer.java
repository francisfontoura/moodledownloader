package br.pucrs.inf.moodledownloader.ui.swing.models.tree;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class IconTreeCellRenderer extends DefaultTreeCellRenderer {

    protected IconTreeCellRenderer() {
        super();
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (value instanceof IconTreeNode) {
            ImageIcon icon = ((IconTreeNode) value).getIcon();
            if (icon != null) {
                setIcon(icon);
            }
        }
        return this;
    }

}
