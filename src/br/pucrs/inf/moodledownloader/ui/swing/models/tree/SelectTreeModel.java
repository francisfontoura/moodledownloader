package br.pucrs.inf.moodledownloader.ui.swing.models.tree;

import br.pucrs.inf.moodledownloader.ui.swing.constants.SelectTreeCards;
import br.pucrs.inf.moodledownloader.ui.swing.constants.SelectTreeProperties;
import br.pucrs.inf.moodledownloader.ui.swing.models.PropertyChangeable;
import javax.swing.tree.TreePath;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class SelectTreeModel extends PropertyChangeable {

    private final SelectTreeSelectionModel selectionModel;
    private final IconTreeCellRenderer cellRenderer;

    private SelectTreeCards southPanelCard;
    private boolean buttonSalvarEnabled;
    private String progressBarText;
    private int progress;

    public SelectTreeModel() {
        selectionModel = new SelectTreeSelectionModel();
        cellRenderer = new IconTreeCellRenderer();
        southPanelCard = SelectTreeCards.CONTROL;
        progressBarText = "Conectando...";
        progress = 0;
    }

    public SelectTreeSelectionModel getSelectionModel() {
        return selectionModel;
    }

    public IconTreeCellRenderer getCellRenderer() {
        return cellRenderer;
    }

    public int getSelectionCount() {
        return selectionModel.getSelectionCount();
    }

    public TreePath[] getSelectionPaths() {
        return selectionModel.getSelectionPaths();
    }
    
    public boolean isSelectionEmpty() {
        return selectionModel.isSelectionEmpty();
    }

    public SelectTreeCards getSouthPanelCard() {
        return southPanelCard;
    }

    public void setSouthPanelCard(SelectTreeCards southPanelCard) {
        SelectTreeCards old = this.southPanelCard;
        this.southPanelCard = southPanelCard;
        firePropertyChange(SelectTreeProperties.SOUTH_PANEL_CARD.name(), old, this.southPanelCard);
    }

    public boolean isButtonSalvarEnabled() {
        return buttonSalvarEnabled;
    }

    public void setButtonSalvarEnabled(boolean buttonSalvarEnabled) {
        boolean old = this.buttonSalvarEnabled;
        this.buttonSalvarEnabled = buttonSalvarEnabled;
        firePropertyChange(SelectTreeProperties.BUTTON_SALVAR_ENABLED.name(), old, this.buttonSalvarEnabled);
    }

    public String getProgressBarText() {
        return progressBarText;
    }

    public void setProgressBarText(String progressBarText) {
        String old = this.progressBarText;
        this.progressBarText = progressBarText;
        firePropertyChange(SelectTreeProperties.PROGRESS_TEXT.name(), old, this.progressBarText);
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        int old = this.progress;
        this.progress = progress;
        firePropertyChange(SelectTreeProperties.PROGRESS.name(), old, this.progress);
    }

}
