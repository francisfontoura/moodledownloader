package br.pucrs.inf.moodledownloader.ui.swing.models.tree;

import java.util.Enumeration;
import java.util.Iterator;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class SelectTreeSelectionModel extends DefaultTreeSelectionModel {

    protected SelectTreeSelectionModel() {
        super();
    }

    @Override
    public void setSelectionPath(TreePath path) {
        super.setSelectionPath(path);
        this.addParentPaths(path);
        this.addChildPaths(path);
    }

    private void addParentPaths(TreePath target) {
        TreePath parent = target.getParentPath();
        if (parent != null) {
            super.addSelectionPath(parent);
            this.addParentPaths(parent);
        }
    }

    private void addChildPaths(TreePath target) {
        Iterator<TreePath> it = this.childPathsIterator(target);
        while (it.hasNext()) {
            TreePath child = it.next();
            super.addSelectionPath(child);
            this.addChildPaths(child);
        }
    }

    @Override
    public void addSelectionPath(TreePath path) {
        super.addSelectionPath(path);
        this.addParentPaths(path);
        this.addChildPaths(path);
    }

    @Override
    public void removeSelectionPath(TreePath path) {
        super.removeSelectionPath(path);
        TreeNode node = (TreeNode) path.getLastPathComponent();
        Enumeration<TreeNode> children = node.children();
        while (children.hasMoreElements()) {
            TreePath child = path.pathByAddingChild(children.nextElement());
            this.removeSelectionPath(child);
        }
        Iterator<TreePath> it = this.childPathsIterator(path);
        while (it.hasNext()) {
            this.removeSelectionPath(it.next());
        }
    }

    private Iterator<TreePath> childPathsIterator(final TreePath parent) {
        return new Iterator<TreePath>() {

            TreeNode node = (TreeNode) parent.getLastPathComponent();
            Enumeration<TreeNode> children = node.children();

            @Override
            public boolean hasNext() {
                return children.hasMoreElements();
            }

            @Override
            public TreePath next() {
                return parent.pathByAddingChild(children.nextElement());
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
    }
}
