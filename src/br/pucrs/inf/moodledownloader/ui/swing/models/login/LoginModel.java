package br.pucrs.inf.moodledownloader.ui.swing.models.login;

import br.pucrs.inf.moodledownloader.service.constants.MoodleProtocols;
import br.pucrs.inf.moodledownloader.ui.swing.constants.LoginProperties;
import br.pucrs.inf.moodledownloader.ui.swing.models.PropertyChangeable;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class LoginModel extends PropertyChangeable {

    private final ComboBoxModel protocolModel;
    private final Document hostModel, userModel, passModel;
    private boolean submitButtonVisible, progressBarVisible, progressBarIndeterminate;
    private String progressBarText;
    private int progress;

    public LoginModel() {
        this.protocolModel = new DefaultComboBoxModel(MoodleProtocols.values());
        this.hostModel = new PlainDocument();
        try {
            this.hostModel.insertString(0, "moodle.pucrs.br", null);
        } catch (BadLocationException ex) {
        }
        this.userModel = new PlainDocument();
        this.passModel = new PlainDocument();
        this.submitButtonVisible = true;
        this.progressBarVisible = false;
        this.progressBarIndeterminate = true;
        this.progressBarText = "Conectando...";
        this.progress = 0;
    }

    public ComboBoxModel getProtocolModel() {
        return protocolModel;
    }

    public Document getHostModel() {
        return hostModel;
    }

    public Document getUserModel() {
        return userModel;
    }

    public Document getPassModel() {
        return passModel;
    }

    public String getProtocol() {
        return ((MoodleProtocols) protocolModel.getSelectedItem()).name();
    }

    public String getHost() {
        try {
            return hostModel.getText(0, hostModel.getLength());
        } catch (BadLocationException ex) {
            return null;
        }
    }

    public String getUser() {
        try {
            return userModel.getText(0, userModel.getLength());
        } catch (BadLocationException ex) {
            return null;
        }
    }

    public String getPassword() {
        try {
            return passModel.getText(0, passModel.getLength());
        } catch (BadLocationException ex) {
            return null;
        }
    }

    public boolean isSubmitButtonVisible() {
        return submitButtonVisible;
    }

    public boolean isProgressBarVisible() {
        return progressBarVisible;
    }

    public boolean isProgressBarIndeterminate() {
        return progressBarIndeterminate;
    }

    public String getProgressBarText() {
        return progressBarText;
    }

    public int getProgress() {
        return progress;
    }

    public void setSubmitButtonVisible(boolean submitButtonVisible) {
        boolean old = this.submitButtonVisible;
        this.submitButtonVisible = submitButtonVisible;
        firePropertyChange(LoginProperties.SUBMIT_VISIBLE.name(), old, this.submitButtonVisible);
    }

    public void setProgressBarVisible(boolean progressBarVisible) {
        boolean old = this.progressBarVisible;
        this.progressBarVisible = progressBarVisible;
        firePropertyChange(LoginProperties.PROGRESS_VISIBLE.name(), old, this.progressBarVisible);
    }

    public void setProgressBarIndeterminate(boolean progressBarIndeterminate) {
        boolean old = this.progressBarIndeterminate;
        this.progressBarIndeterminate = progressBarIndeterminate;
        firePropertyChange(LoginProperties.PROGRESS_INDETERMINATE.name(), old, this.progressBarIndeterminate);
    }

    public void setProgressBarText(String progressBarText) {
        String old = this.progressBarText;
        this.progressBarText = progressBarText;
        firePropertyChange(LoginProperties.PROGRESS_TEXT.name(), old, this.progressBarText);
    }

    public void setProgress(int progress) {
        int old = this.progress;
        this.progress = progress;
        firePropertyChange(LoginProperties.PROGRESS.name(), old, this.progress);
    }

    public void setDisposed() {
        firePropertyChange(LoginProperties.DISPOSED.name(), false, true);
    }

}
