package br.pucrs.inf.moodledownloader.ui.swing.models.tree;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class IconTreeNode extends DefaultMutableTreeNode {

    private final ImageIcon icon;

    public IconTreeNode(ImageIcon icon, Object userObject) {
        super(userObject);
        this.icon = icon;
    }

    public ImageIcon getIcon() {
        return icon;
    }
}
