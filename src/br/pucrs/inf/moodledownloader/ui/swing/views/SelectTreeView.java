package br.pucrs.inf.moodledownloader.ui.swing.views;

import br.pucrs.inf.moodledownloader.ui.swing.constants.SelectTreeCards;
import br.pucrs.inf.moodledownloader.ui.swing.constants.SelectTreeProperties;
import br.pucrs.inf.moodledownloader.ui.swing.controllers.SelectTreeController;
import br.pucrs.inf.moodledownloader.ui.swing.models.tree.SelectTreeModel;
import java.awt.CardLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JFrame;
import javax.swing.tree.TreeModel;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class SelectTreeView extends javax.swing.JFrame implements PropertyChangeListener {
    
    private final TreeModel treeModel;
    private SelectTreeModel model;
    private SelectTreeController controller;
    
    private void myInit() {
        model = new SelectTreeModel();
        model.addPropertyChangeListener(this);
        controller = new SelectTreeController(model);
        
        jTreeActivities.setModel(treeModel);
        jTreeActivities.setCellRenderer(model.getCellRenderer());
        jTreeActivities.setSelectionModel(model.getSelectionModel());
        jTreeActivities.addTreeSelectionListener(controller);
        
        jPanelSouth.add(jPanelControl, SelectTreeCards.CONTROL.name());
        jPanelSouth.add(jProgressBar, SelectTreeCards.PROGRESS.name());
        ((CardLayout) jPanelSouth.getLayout()).show(jPanelSouth, model.getSouthPanelCard().name());
        
        jButtonSalvar.addActionListener(controller);
        jButtonSalvar.setEnabled(model.isButtonSalvarEnabled());
        
        jProgressBar.setString(model.getProgressBarText());
        jProgressBar.setStringPainted(true);
    }
    
    public SelectTreeView(TreeModel treeModel) {
        this.treeModel = treeModel;
        initComponents();
        myInit();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelControl = new javax.swing.JPanel();
        jButtonSalvar = new javax.swing.JButton();
        jProgressBar = new javax.swing.JProgressBar();
        jScrollPaneTree = new javax.swing.JScrollPane();
        jTreeActivities = new javax.swing.JTree();
        jLabelInfo = new javax.swing.JLabel();
        jPanelSouth = new javax.swing.JPanel();

        jButtonSalvar.setText("Salvar selecionados...");
        jPanelControl.add(jButtonSalvar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        jScrollPaneTree.setViewportView(jTreeActivities);

        getContentPane().add(jScrollPaneTree, java.awt.BorderLayout.CENTER);

        jLabelInfo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelInfo.setText("Selecione os recursos a serem salvos localmente. Segure a tecla [Ctrl] ou [⌘command] para seleção múltipla.");
        getContentPane().add(jLabelInfo, java.awt.BorderLayout.PAGE_START);

        jPanelSouth.setLayout(new java.awt.CardLayout());
        getContentPane().add(jPanelSouth, java.awt.BorderLayout.PAGE_END);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JLabel jLabelInfo;
    private javax.swing.JPanel jPanelControl;
    private javax.swing.JPanel jPanelSouth;
    private javax.swing.JProgressBar jProgressBar;
    private javax.swing.JScrollPane jScrollPaneTree;
    private javax.swing.JTree jTreeActivities;
    // End of variables declaration//GEN-END:variables

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getNewValue().equals(evt.getOldValue())) {
            return;
        }
        switch (SelectTreeProperties.valueOf(evt.getPropertyName())) {
            case SOUTH_PANEL_CARD:
                ((CardLayout) jPanelSouth.getLayout()).show(jPanelSouth, ((SelectTreeCards) evt.getNewValue()).name());
                break;
            case BUTTON_SALVAR_ENABLED:
                jButtonSalvar.setEnabled((Boolean) evt.getNewValue());
                break;
            case PROGRESS_TEXT:
                jProgressBar.setString((String) evt.getNewValue());
                break;
            case PROGRESS:
                jProgressBar.setValue((Integer) evt.getNewValue());
                break;
        }
    }
    
}
