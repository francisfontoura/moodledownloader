package br.pucrs.inf.moodledownloader.ui.swing.views;

import br.pucrs.inf.moodledownloader.ui.swing.constants.LoginProperties;
import br.pucrs.inf.moodledownloader.ui.swing.controllers.LoginController;
import br.pucrs.inf.moodledownloader.ui.swing.models.login.LoginModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class LoginView extends javax.swing.JFrame implements PropertyChangeListener {

    private LoginModel model;
    private LoginController controller;

    public LoginView() {
        initComponents();
        myInit();
    }

    private void myInit() {
        model = new LoginModel();
        model.addPropertyChangeListener(this);
        controller = new LoginController(model);
        jComboBoxProto.setModel(model.getProtocolModel());
        jTextFieldHost.setDocument(model.getHostModel());
        jTextFieldUser.setDocument(model.getUserModel());
        jTextFieldPass.setDocument(model.getPassModel());
        jButtonLogin.setVisible(model.isSubmitButtonVisible());
        jButtonLogin.addActionListener(controller);
        jProgressBar.setVisible(model.isProgressBarVisible());
        jProgressBar.setIndeterminate(model.isProgressBarIndeterminate());
        jProgressBar.setString(model.getProgressBarText());
        jProgressBar.setValue(model.getProgress());
        rootPane.setDefaultButton(jButtonLogin);
        jTextFieldUser.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabelHost = new javax.swing.JLabel();
        jLabelUser = new javax.swing.JLabel();
        jLabelPass = new javax.swing.JLabel();
        jTextFieldHost = new javax.swing.JTextField();
        jTextFieldUser = new javax.swing.JTextField();
        jTextFieldPass = new javax.swing.JPasswordField();
        jButtonLogin = new javax.swing.JButton();
        jComboBoxProto = new javax.swing.JComboBox();
        jLabelProto = new javax.swing.JLabel();
        jProgressBar = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        java.awt.GridBagLayout layout = new java.awt.GridBagLayout();
        layout.columnWidths = new int[] {0, 0, 0};
        layout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        getContentPane().setLayout(layout);

        jLabelHost.setText("Host/IP:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(jLabelHost, gridBagConstraints);

        jLabelUser.setText("Usuário:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(jLabelUser, gridBagConstraints);

        jLabelPass.setText("Senha:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(jLabelPass, gridBagConstraints);

        jTextFieldHost.setPreferredSize(new java.awt.Dimension(120, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(jTextFieldHost, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(jTextFieldUser, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(jTextFieldPass, gridBagConstraints);

        jButtonLogin.setText("Login");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        getContentPane().add(jButtonLogin, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        getContentPane().add(jComboBoxProto, gridBagConstraints);

        jLabelProto.setText("Protocolo:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        getContentPane().add(jLabelProto, gridBagConstraints);

        jProgressBar.setString("Conectando...");
        jProgressBar.setStringPainted(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        getContentPane().add(jProgressBar, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonLogin;
    private javax.swing.JComboBox jComboBoxProto;
    private javax.swing.JLabel jLabelHost;
    private javax.swing.JLabel jLabelPass;
    private javax.swing.JLabel jLabelProto;
    private javax.swing.JLabel jLabelUser;
    private javax.swing.JProgressBar jProgressBar;
    private javax.swing.JTextField jTextFieldHost;
    private javax.swing.JPasswordField jTextFieldPass;
    private javax.swing.JTextField jTextFieldUser;
    // End of variables declaration//GEN-END:variables

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getNewValue().equals(evt.getOldValue())) {
            return;
        }
        switch (LoginProperties.valueOf(evt.getPropertyName())) {
            case SUBMIT_VISIBLE:
                jButtonLogin.setVisible((Boolean) evt.getNewValue());
                break;
            case PROGRESS_VISIBLE:
                jProgressBar.setVisible((Boolean) evt.getNewValue());
                break;
            case PROGRESS_INDETERMINATE:
                jProgressBar.setIndeterminate((Boolean) evt.getNewValue());
                break;
            case PROGRESS_TEXT:
                jProgressBar.setString((String) evt.getNewValue());
                break;
            case PROGRESS:
                jProgressBar.setValue((Integer) evt.getNewValue());
                break;
            case DISPOSED:
                this.dispose();
        }
    }

}
