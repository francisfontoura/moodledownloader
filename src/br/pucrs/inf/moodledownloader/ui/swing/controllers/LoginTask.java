package br.pucrs.inf.moodledownloader.ui.swing.controllers;

import br.pucrs.inf.moodledownloader.service.constants.MoodleProtocols;
import br.pucrs.inf.moodledownloader.service.images.ImageHelperFactory;
import br.pucrs.inf.moodledownloader.service.parser.Connection;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import br.pucrs.inf.moodledownloader.service.parser.objects.MyMoodle;
import br.pucrs.inf.moodledownloader.ui.swing.models.login.LoginModel;
import br.pucrs.inf.moodledownloader.ui.swing.models.tree.IconTreeNode;
import br.pucrs.inf.moodledownloader.ui.swing.views.SelectTreeView;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class LoginTask extends SwingWorker<TreeModel, Void> {

    private final LoginModel model;
    private int done, total;

    public LoginTask(LoginModel model) {
        this.model = model;
    }

    public int getDone() {
        return done;
    }

    public int getTotal() {
        return total;
    }

    private void updateProgress() {
        setProgress(100 * done / total);
    }

    private DefaultMutableTreeNode getNode(MoodleObject parent) throws Exception {
        List<MoodleObject> childs = parent.getChilds();
        total += childs.size();
        updateProgress();
        DefaultMutableTreeNode node;
        if (parent.getIconUrl() != null) {
            ImageIcon icon = new ImageIcon(ImageHelperFactory.getInstance().urlToImage(parent.getIconUrl()));
            node = new IconTreeNode(icon, parent);
        } else {
            node = new DefaultMutableTreeNode(parent);
        }
        for (MoodleObject child : childs) {
            node.add(getNode(child));
            done++;
            updateProgress();
        }
        return node;
    }

    @Override
    protected TreeModel doInBackground() throws Exception {
        MoodleProtocols proto = MoodleProtocols.valueOf(model.getProtocol());
        String host = model.getHost();
        String user = model.getUser();
        String pass = model.getPassword();
        Connection.init(proto, host, user, pass, 10000);
        MyMoodle my = new MyMoodle(Connection.getInstance());
        return new DefaultTreeModel(getNode(my));
    }

    @Override
    protected void done() {
        try {
            TreeModel treeModel = this.get();
            model.setDisposed();
            new SelectTreeView(treeModel).setVisible(true);
        } catch (InterruptedException | ExecutionException ex) {
            JOptionPane.showMessageDialog(null, ex.getCause().getMessage());
            model.setProgressBarVisible(false);
            model.setSubmitButtonVisible(true);
        }
    }

}
