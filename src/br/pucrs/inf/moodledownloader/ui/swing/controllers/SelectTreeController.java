package br.pucrs.inf.moodledownloader.ui.swing.controllers;

import br.pucrs.inf.moodledownloader.service.parser.deprecated.Saveable;
import br.pucrs.inf.moodledownloader.ui.swing.constants.SelectTreeCards;
import br.pucrs.inf.moodledownloader.ui.swing.models.tree.SelectTreeModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class SelectTreeController
        extends SwingWorker<Void, Void>
        implements ActionListener, PropertyChangeListener, TreeSelectionListener {
    
    private final SelectTreeModel model;
    private final JFileChooser fc;
    
    public SelectTreeController(SelectTreeModel model) {
        this.model = model;
        fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
    
    private int done, total;
    
    private void updateProgress() {
        setProgress(100 * done / total);
    }
    
    @Override
    protected Void doInBackground() throws Exception {
        setProgress(0);
        total = model.getSelectionCount();
        for (TreePath selected : model.getSelectionPaths()) {
            try {
                Saveable node = (Saveable) ((DefaultMutableTreeNode) selected.getLastPathComponent()).getUserObject();
                node.downloadTo(fc.getSelectedFile().toPath());
                done++;
                updateProgress();
            } catch (ClassCastException ex) {
                total--;
                updateProgress();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
        return null;
    }
    
    @Override
    protected void done() {
        JOptionPane.showMessageDialog(null, "Cópia concluída com sucesso!");
        model.setSouthPanelCard(SelectTreeCards.CONTROL);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        File file = null;
        boolean error, ok;
        do {
            error = false;
            fc.setCurrentDirectory(file);
            int val = fc.showSaveDialog(null);
            ok = (val == JFileChooser.APPROVE_OPTION);
            if (ok) {
                file = fc.getSelectedFile();
                if (!file.canWrite()) {
                    JOptionPane.showMessageDialog(null, "Não pode escrever no diretório selecionado.");
                    error = true;
                }
            }
        } while (error);
        if (ok) {
            model.setSouthPanelCard(SelectTreeCards.PROGRESS);
            addPropertyChangeListener(this);
            execute();
        }
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("progress")) {
            int progress = (Integer) evt.getNewValue();
            model.setProgressBarText(String.format("Salvando... %d%% (%d/%d)", progress, done, total));
            model.setProgress(progress);
        }
    }
    
    @Override
    public void valueChanged(TreeSelectionEvent e) {
        model.setButtonSalvarEnabled(!model.isSelectionEmpty());
    }
    
}
