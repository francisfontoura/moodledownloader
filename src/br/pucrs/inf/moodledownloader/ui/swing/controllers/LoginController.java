package br.pucrs.inf.moodledownloader.ui.swing.controllers;

import br.pucrs.inf.moodledownloader.ui.swing.models.login.LoginModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class LoginController implements ActionListener, PropertyChangeListener {

    private final LoginModel model;
    private LoginTask task;

    public LoginController(LoginModel model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        model.setSubmitButtonVisible(false);
        model.setProgressBarVisible(true);
        task = new LoginTask(model);
        task.addPropertyChangeListener(this);
        task.execute();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("progress")) {
            int progress = (Integer) evt.getNewValue();
            model.setProgressBarIndeterminate(false);
            model.setProgressBarText(String.format("Carregando... (%d/%d)", task.getDone(), task.getTotal()));
            model.setProgress(progress);
        }
    }

}
