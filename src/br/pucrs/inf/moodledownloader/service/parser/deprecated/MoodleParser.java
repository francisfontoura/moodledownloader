package br.pucrs.inf.moodledownloader.service.parser.deprecated;

import br.pucrs.inf.moodledownloader.service.constants.MoodleProtocols;
import br.pucrs.inf.moodledownloader.service.constants.HttpRequestMethods;
import br.pucrs.inf.moodledownloader.service.http.SimpleHttpClient;
import br.pucrs.inf.moodledownloader.service.images.ImageHelperFactory;
import java.awt.Image;
import java.io.File;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class MoodleParser {

    private static MoodleProtocols PROTO;
    private static String HOST;
    private static final int TIMEOUT = 10000;
    private static final String ACTION = "/login/index.php";
    private static final String VAR_USER = "username";
    private static final String VAR_PASS = "password";
    private static final String ID_ERROR = "loginerrormessage";
    private static final String CLASS_COURSE = "coursebox";
    private static final String SLASH = System.getProperty("file.separator");

    private final Document doc;

    public MoodleParser(MoodleProtocols proto, String host, String username, String password) throws Exception {
        PROTO = proto;
        HOST = host;
        SimpleHttpClient client = SimpleHttpClient.custom()
                .useCookies()
                .setTimeout(TIMEOUT)
                .setMethod(HttpRequestMethods.POST)
                .setAction(PROTO + "://" + HOST + ACTION)
                .addParam(VAR_USER, username)
                .addParam(VAR_PASS, password)
                .build();
        doc = Jsoup.parse(client.toString());
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        Element error = doc.getElementById(ID_ERROR);
        if (error != null) {
            throw new Exception(error.html());
        }
    }

    public List<Course> getCourses() throws Exception {
        List<Course> list = new LinkedList();
        Iterator<Element> it = doc.getElementsByClass(CLASS_COURSE).iterator();
        while (it.hasNext()) {
            list.add(new Course(it.next()));
        }
        return list;
    }

    public static class Course {

        private static final String ACTION = "/course/view.php";
        private static final String VAR_ID = "id";
        private static final String CLASS_ACTIVITY = "activityinstance";

        private final Element el;
        private final Document doc;

        public Course(Element jsoupElement) throws Exception {
            el = jsoupElement;
            doc = getDocument();
        }

        private Document getDocument() throws Exception {
            SimpleHttpClient client = SimpleHttpClient.custom()
                    .useCookies()
                    .setAction(PROTO + "://" + HOST + ACTION)
                    .addParam(VAR_ID, String.valueOf(getId()))
                    .build();
            Document doc = Jsoup.parse(client.toString());
            doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
            return doc;
        }

        public Element getAnchor() {
            return el.getElementsByTag("a").first();
        }

        public final int getId() {
            return Integer.parseInt(getAnchor().attr("href").split("=")[1]);
        }

        public String getName() {
            return getAnchor().html();
        }

        public List<Activity> getActivities() throws Exception {
            List<Activity> list = new LinkedList();
            Iterator<Element> it = doc.getElementsByClass(CLASS_ACTIVITY).iterator();
            while (it.hasNext()) {
                Activity activity = new Activity(it.next());
                if (activity.getType().equals(Activity.Type.FORUM)) {
                    activity = new Forum(activity);
                }
                list.add(activity);
            }
            return list;
        }

        @Override
        public String toString() {
            return getName();
        }

        public static class Activity {

            private static final String CLASS_ICON = "activityicon";
            private static final String CLASS_NAME = "instancename";
            private static final String CLASS_NAME_REMOVE = "accesshide";

            public enum Type {

                FORUM, RESOURCE, URL, PAGE, ASSIGN, FOLDER, CHOICE;

                @Override
                public String toString() {
                    return super.toString().toLowerCase();
                }
            }

            private final Element el;

            public Activity(Element jsoupElement) {
                el = jsoupElement;
            }

            public Element getAnchor() {
                return el.getElementsByTag("a").first();
            }

            public Image getIcon() throws Exception {
                return ImageHelperFactory.getInstance().urlToImage(getAnchor().getElementsByClass(CLASS_ICON).first().attr("src"));
            }

            public final Type getType() {
                return Type.valueOf(getAnchor().attr("href").split("/")[4].toUpperCase());
            }

            public int getId() {
                return Integer.parseInt(getAnchor().attr("href").split("=")[1]);
            }

            public String getName() {
                Element el = getAnchor().getElementsByClass(CLASS_NAME).first();
                el.getElementsByClass(CLASS_NAME_REMOVE).remove();
                return el.html();
            }

            @Override
            public String toString() {
                return getName();
            }
        }

        public static class Forum extends Activity {

            private static final String ACTION = "/mod/forum/view.php";
            private static final String VAR_ID = "id";
            private static final String CLASS_TOPIC = "discussion";

            private final Document doc;

            public Forum(Activity activity) throws Exception {
                super(activity.el);
                doc = getDocument();
            }

            private Document getDocument() throws Exception {
                SimpleHttpClient client = SimpleHttpClient.custom()
                        .useCookies()
                        .setAction(PROTO + "://" + HOST + ACTION)
                        .addParam(VAR_ID, String.valueOf(getId()))
                        .build();
                Document doc = Jsoup.parse(client.toString());
                doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
                return doc;
            }

            public List<Topic> getTopics() throws Exception {
                List<Topic> list = new LinkedList();
                Iterator<Element> it = doc.getElementsByClass(CLASS_TOPIC).iterator();
                while (it.hasNext()) {
                    list.add(new Topic(it.next()));
                }
                return list;
            }

            public static class Topic implements Saveable {

                private static final String ACTION = "/mod/forum/discuss.php";
                private static final String VAR_ID = "d";
                private static final String INDEX_HTML = "index.html";

                private final Element el;

                public Topic(Element jsoupElement) {
                    el = jsoupElement;
                }

                private Document getDocument() throws Exception {
                    SimpleHttpClient client = SimpleHttpClient.custom()
                            .useCookies()
                            .setAction(PROTO + "://" + HOST + ACTION)
                            .addParam(VAR_ID, String.valueOf(getId()))
                            .build();
                    Document doc = Jsoup.parse(client.toString());
                    doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
                    return doc;
                }

                public Element getAnchor() {
                    return el.getElementsByTag("a").first();
                }

                public final int getId() {
                    return Integer.parseInt(getAnchor().attr("href").split("=")[1]);
                }

                public String getName() {
                    return getAnchor().html();
                }

                @Override
                public String toString() {
                    return getName();
                }

                @Override
                public void downloadTo(Path path) throws Exception {
                    Document doc = getDocument();
                    downloadAttachments(doc, path.resolve("" + getId()));
                    downloadHtml(doc, path.resolve(getId() + ".html").toFile());
                }

                @Override
                public void relink() {
                    getAnchor().attr("href", getId() + ".html");
                }
            }
        }
    }

    private static void downloadHtml(Document doc, File file) throws Exception {
        try (PrintWriter out = new PrintWriter(file)) {
            out.print(doc.toString());
        }
    }

    private static void downloadAttachments(Document doc, Path path) throws Exception {
        downloadAttachments(doc, path, true);
        downloadAttachments(doc, path, false);
    }

    private static void downloadAttachments(Document doc, Path path, boolean isImage) throws Exception {
        String CLASS = isImage ? "fullpost" : "attachments";
        String TAG = isImage ? "img" : "a";
        String ATTR = isImage ? "src" : "href";
        Element content = doc.getElementsByClass(CLASS).first();
        if (content != null) {
            Iterator<Element> it = content.getElementsByTag(TAG).iterator();
            while (it.hasNext()) {
                Element elem = it.next();
                String url = elem.attr(ATTR);
                String filename = URLDecoder.decode(url.substring(url.lastIndexOf("/") + 1), "UTF8");
                File target = path.resolve(filename).toFile();
                SimpleHttpClient.custom()
                        .useCookies()
                        .setAction(url)
                        .build()
                        .toFile(target);
                int count = path.getNameCount();
                elem.attr(ATTR, path.subpath(count - 1, count).resolve(filename).toString());
            }
        }
    }
}
