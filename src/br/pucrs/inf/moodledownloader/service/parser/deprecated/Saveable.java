package br.pucrs.inf.moodledownloader.service.parser.deprecated;

import java.nio.file.Path;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public interface Saveable {
    
    void downloadTo(Path path) throws Exception;

    void relink();
    
}
