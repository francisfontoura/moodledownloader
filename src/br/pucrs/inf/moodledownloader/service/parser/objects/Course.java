package br.pucrs.inf.moodledownloader.service.parser.objects;

import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import br.pucrs.inf.moodledownloader.service.parser.Connection;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import java.net.URL;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.text.WordUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Course implements MoodleObject {

    private final Element el;

    public Course(Element jsoupElement) throws Exception {
        el = jsoupElement;
    }

    @Override
    public String getIconUrl() {
        return null;
    }

    @Override
    public String getLabel() {
        return el.getElementsByTag("a").first().text();
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        List<MoodleObject> activities = new LinkedList();
        Document doc = Jsoup.parse(getHtml());
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        Element section = doc.getElementsByClass(MoodleConstants.CLASS_TABS_SECTION).first();
        if (section != null) {
            section.remove();
        }
        Iterator<Element> it = doc
                .getElementsByClass(MoodleConstants.CLASS_ACTIVITY)
                .iterator();
        while (it.hasNext()) {
            activities.add(ActivityFactory.newInstance(it.next()));
        }
        if (section != null) {
            Element tabs = section.getElementsByClass(MoodleConstants.CLASS_TABS).first();
            if (tabs != null) {
                it = tabs.getElementsByTag("li").iterator();
                while (it.hasNext()) {
                    activities.add(new Tab(it.next()));
                }
            }
        }
        return activities;
    }

    @Override
    public String getHtml() throws Exception {
        return Connection.getInstance()
                .navigateTo(el.getElementsByTag("a").attr("href"))
                .getHtml();
    }

    @Override
    public void setDownloadPath(Path path
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void download() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void relink() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return getLabel();
    }

}
