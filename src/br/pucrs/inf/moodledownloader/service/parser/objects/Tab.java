package br.pucrs.inf.moodledownloader.service.parser.objects;

import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import br.pucrs.inf.moodledownloader.service.parser.Connection;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Tab implements MoodleObject {

    private final Element el;
    private final String html;

    public Tab(Element jsoupElement) throws Exception {
        el = jsoupElement;
        html = el.hasClass(MoodleConstants.CLASS_THIS_TAB)
                ? Connection.getInstance().getHtml()
                : Connection
                .getInstance()
                .navigateTo(el.getElementsByTag("a").first().attr("href"))
                .getHtml();
    }

    @Override
    public String getIconUrl() {
        return Jsoup.parse(html)
                .getElementsByClass(MoodleConstants.CLASS_TABS_SECTION)
                .first() != null
                ? "https://cdn1.iconfinder.com/data/icons/Futurosoft%20Icons%200.5.2/22x22/actions/tab.png"
                : "https://cdn1.iconfinder.com/data/icons/Vista-Inspirate_1.0/22x22/actions/tab_remove.png";
    }

    @Override
    public String getLabel() {
        return el.text();
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        List<MoodleObject> activities = new LinkedList();
        Element section = Jsoup.parse(html)
                .getElementsByClass(MoodleConstants.CLASS_TABS_SECTION)
                .first();
        if (section != null) {
            Iterator<Element> it = section
                    .getElementsByClass(MoodleConstants.CLASS_ACTIVITY)
                    .iterator();
            while (it.hasNext()) {
                activities.add(ActivityFactory.newInstance(it.next()));
            }
        }
        return activities;
    }

    @Override
    public String getHtml() throws Exception {
        return html;
    }

    @Override
    public void setDownloadPath(Path path) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void download() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void relink() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return getLabel();
    }

}
