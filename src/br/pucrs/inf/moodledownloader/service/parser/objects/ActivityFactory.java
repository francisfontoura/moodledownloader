package br.pucrs.inf.moodledownloader.service.parser.objects;

import java.net.URL;
import org.apache.commons.lang3.text.WordUtils;
import org.jsoup.nodes.Element;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public abstract class ActivityFactory {

    private static final String PKG_ACTIVITIES = ActivityFactory.class.getPackage().getName() + ".activity";

    public static Activity newInstance(Element jsoupElement) throws Exception {
        return (Activity) Class.forName(
                PKG_ACTIVITIES + "."
                + WordUtils.capitalize(
                        new URL(
                                jsoupElement
                                .getElementsByTag("a")
                                .first()
                                .attr("href")
                        ).getPath().split("/")[2]
                )
        ).getConstructors()[0].newInstance(jsoupElement);
    }

}
