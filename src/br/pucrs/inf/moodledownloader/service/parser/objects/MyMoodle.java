package br.pucrs.inf.moodledownloader.service.parser.objects;

import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import br.pucrs.inf.moodledownloader.service.parser.Connection;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class MyMoodle implements MoodleObject {

    private final Connection moodle;

    public MyMoodle(Connection moodle) throws Exception {
        this.moodle = moodle;
    }

    @Override
    public String getIconUrl() {
        return null;
    }

    @Override
    public String getLabel() {
        return moodle.getHost();
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        List<MoodleObject> courses = new LinkedList();
        Document doc = Jsoup.parse(getHtml());
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        Iterator<Element> it = doc.getElementsByClass(MoodleConstants.CLASS_COURSE).iterator();
        while (it.hasNext()) {
            courses.add(new Course(it.next()));
        }
        return courses;
    }

    @Override
    public String getHtml() {
        return moodle.getHtml();
    }

    @Override
    public void setDownloadPath(Path path) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void download() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void relink() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return getLabel();
    }

}
