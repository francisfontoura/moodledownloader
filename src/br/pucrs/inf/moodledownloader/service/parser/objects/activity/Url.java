package br.pucrs.inf.moodledownloader.service.parser.objects.activity;

import br.pucrs.inf.moodledownloader.service.parser.objects.Activity;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.nodes.Element;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Url extends Activity {

    public Url(Element jsoupElement) throws Exception {
        super(jsoupElement);
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        return new LinkedList();
    }

}
