package br.pucrs.inf.moodledownloader.service.parser.objects;

import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import br.pucrs.inf.moodledownloader.service.parser.Connection;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import java.nio.file.Path;
import java.util.List;
import org.jsoup.nodes.Element;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public abstract class Activity implements MoodleObject {

    private final Element el;

    public Activity(Element jsoupElement) throws Exception {
        el = jsoupElement;
    }

    @Override
    public String getIconUrl() {
        return el.getElementsByClass(MoodleConstants.CLASS_ACTIVITY_ICON).first().attr("src");
    }

    @Override
    public String getLabel() {
        Element el = this.el.getElementsByClass(MoodleConstants.CLASS_ACTIVITY_NAME).first();
        el.getElementsByClass(MoodleConstants.CLASS_BLIND).remove();
        return el.text();
    }

    @Override
    public abstract List<MoodleObject> getChilds() throws Exception;

    @Override
    public String getHtml() throws Exception {
        return Connection.getInstance()
                .navigateTo(el.getElementsByTag("a").first().attr("href"))
                .getHtml();
    }

    @Override
    public void setDownloadPath(Path path) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void download() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void relink() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return getLabel();
    }

}
