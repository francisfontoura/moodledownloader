package br.pucrs.inf.moodledownloader.service.parser.objects.activity;

import br.pucrs.inf.moodledownloader.service.parser.objects.Activity;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import java.net.URL;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.nodes.Element;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Resource extends Activity {

    public Resource(Element jsoupElement) throws Exception {
        super(jsoupElement);
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        return new LinkedList();
    }

}
