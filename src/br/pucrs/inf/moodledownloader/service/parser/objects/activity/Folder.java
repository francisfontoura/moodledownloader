package br.pucrs.inf.moodledownloader.service.parser.objects.activity;

import br.pucrs.inf.moodledownloader.service.parser.objects.activity.folder.File;
import br.pucrs.inf.moodledownloader.service.parser.objects.Activity;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Folder extends Activity {

    public Folder(Element jsoupElement) throws Exception {
        super(jsoupElement);
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        List<MoodleObject> files = new LinkedList();
        Document doc = Jsoup.parse(getHtml());
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        Iterator<Element> it = doc.getElementsByClass(MoodleConstants.CLASS_FILE).iterator();
        while (it.hasNext()) {
            Element el = it.next();
            if (!el.getElementsByTag("a").isEmpty()) {
                files.add(new File(el));
            }
        }
        return files;
    }

}
