package br.pucrs.inf.moodledownloader.service.parser.objects.activity.forum;

import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.nodes.Element;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Topic implements MoodleObject {
    
    private Element el;

    public Topic(Element jsoupElement) throws Exception {
        el = jsoupElement;
    }

    @Override
    public String getIconUrl() {
        return el.getElementsByClass(MoodleConstants.CLASS_TOPIC_ICON)
                .first()
                .getElementsByTag("img")
                .first()
                .attr("src");
    }

    @Override
    public String getLabel() {
        return el.getElementsByClass(MoodleConstants.CLASS_TOPIC_NAME)
                .first()
                .text();
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        return new LinkedList();
    }

    @Override
    public String getHtml() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setDownloadPath(Path path) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void download() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void relink() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return getLabel();
    }
    
    

}
