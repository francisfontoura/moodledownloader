package br.pucrs.inf.moodledownloader.service.parser.objects.activity;

import br.pucrs.inf.moodledownloader.service.parser.objects.activity.forum.Topic;
import br.pucrs.inf.moodledownloader.service.parser.objects.Activity;
import br.pucrs.inf.moodledownloader.service.parser.MoodleObject;
import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Forum extends Activity {

    public Forum(Element jsoupElement) throws Exception {
        super(jsoupElement);
    }

    @Override
    public List<MoodleObject> getChilds() throws Exception {
        List<MoodleObject> topics = new LinkedList();
        Document doc = Jsoup.parse(getHtml());
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        Iterator<Element> it = doc.getElementsByClass(MoodleConstants.CLASS_TOPIC).iterator();
        while (it.hasNext()) {
            topics.add(new Topic(it.next()));
        }
        return topics;
    }

}
