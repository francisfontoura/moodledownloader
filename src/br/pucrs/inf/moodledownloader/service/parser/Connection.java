package br.pucrs.inf.moodledownloader.service.parser;

import br.pucrs.inf.moodledownloader.service.constants.MoodleConstants;
import br.pucrs.inf.moodledownloader.service.constants.MoodleProtocols;
import br.pucrs.inf.moodledownloader.service.http.SimpleHttpClient;
import br.pucrs.inf.moodledownloader.service.http.builders.SimpleHttpClientBuilder;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class Connection {

    private final MoodleProtocols protocol;
    private final String host;
    private final String username;
    private final String password;
    private final int timeout;
    private String html;

    private static Connection instance;

    private Connection(MoodleProtocols protocol, String host, String username, String password, int timeout) {
        this.protocol = protocol;
        this.host = host;
        this.username = username;
        this.password = password;
        this.timeout = timeout;
    }

    public static void init(MoodleProtocols protocol, String host, String username, String password, int timeout) throws Exception {
        instance = new Connection(protocol, host, username, password, timeout);
        instance.login();
    }

    public static Connection getInstance() {
        return instance;
    }

    public Connection login() throws Exception {
        String action = Jsoup.parse(new URL(protocol.name(), host, -1, ""), 10000)
                .getElementById(MoodleConstants.ID_LOGIN_FORM)
                .attr("action");
        html = SimpleHttpClient.custom()
                .useCookies()
                .setTimeout(timeout)
                .setMethod(MoodleConstants.LOGIN_METHOD)
                .setAction(action)
                .addParam(MoodleConstants.FIELD_USER, username)
                .addParam(MoodleConstants.FIELD_PASS, password)
                .build()
                .toString();
        String error = getLoginError();
        if (error != null) {
            throw new Exception(error);
        }
        return this;
    }

    public Connection navigateTo(String url) throws Exception {
        html = SimpleHttpClient.custom()
                .useCookies()
                .setAction(url)
                .build()
                .toString();
        String error = getLoginError();
        if (error != null) {
            throw new Exception(error);
        }
        return this;
    }

    private String getLoginError() {
        Document doc = Jsoup.parse(html);
        doc.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
        Element error = doc.getElementById(MoodleConstants.ID_ERROR);
        if (error != null) {
            return error.text();
        }
        return null;
    }

    public String getHtml() {
        return html;
    }

    public String getHost() {
        return host;
    }

}
