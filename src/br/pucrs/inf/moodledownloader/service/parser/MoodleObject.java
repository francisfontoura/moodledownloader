package br.pucrs.inf.moodledownloader.service.parser;

import java.nio.file.Path;
import java.util.List;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public interface MoodleObject {

    String getIconUrl();

    String getLabel();

    List<MoodleObject> getChilds() throws Exception;
    
    String getHtml() throws Exception;

    void setDownloadPath(Path path);

    void download();

    void relink();

}
