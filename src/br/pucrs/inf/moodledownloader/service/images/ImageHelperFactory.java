package br.pucrs.inf.moodledownloader.service.images;

import br.pucrs.inf.moodledownloader.service.constants.ImageHelperConfig;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public abstract class ImageHelperFactory {

    public static ImageHelper getInstance() {
        try {
            return (ImageHelper) ImageHelperConfig.IMAGE_HELPER.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            return null;
        }
    }

}
