package br.pucrs.inf.moodledownloader.service.images.impl;

import br.pucrs.inf.moodledownloader.service.images.ImageHelper;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import javax.imageio.ImageIO;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class ImageHelperBatik implements ImageHelper {

    @Override
    public BufferedImage urlToImage(URL url) throws Exception {
        BufferedImage image = ImageIO.read(url);
        if (image == null) { //SVG
            PNGTranscoder transcoder = new PNGTranscoder();
            TranscoderInput input = new TranscoderInput(url.openStream());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            TranscoderOutput output = new TranscoderOutput(stream);
            transcoder.transcode(input, output);
            image = ImageIO.read(new ByteArrayInputStream(stream.toByteArray()));
        }
        return image;
    }

    @Override
    public BufferedImage urlToImage(String url) throws Exception {
        return urlToImage(new URL(url));
    }

}
