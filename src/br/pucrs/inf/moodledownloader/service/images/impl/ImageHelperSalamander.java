package br.pucrs.inf.moodledownloader.service.images.impl;

import br.pucrs.inf.moodledownloader.service.images.ImageHelper;
import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGUniverse;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.imageio.ImageIO;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class ImageHelperSalamander implements ImageHelper {

    @Override
    public BufferedImage urlToImage(URL url) throws Exception {
        BufferedImage image = ImageIO.read(url);
        if (image == null) { //SVG
            SVGUniverse universe = new SVGUniverse();
            SVGDiagram diagram = universe.getDiagram(url.toURI());
            image = new BufferedImage(24, 24, BufferedImage.TYPE_4BYTE_ABGR);
            diagram.render(image.createGraphics());
        }
        return image;
    }

    @Override
    public BufferedImage urlToImage(String url) throws Exception {
        return urlToImage(new URL(url));
    }

}
