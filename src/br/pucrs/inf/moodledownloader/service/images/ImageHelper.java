package br.pucrs.inf.moodledownloader.service.images;

import java.awt.image.BufferedImage;
import java.net.URL;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public interface ImageHelper {

    BufferedImage urlToImage(URL url) throws Exception;

    BufferedImage urlToImage(String url) throws Exception;
    
}
