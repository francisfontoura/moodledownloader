package br.pucrs.inf.moodledownloader.service.constants;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class MoodleConstants {

    public static final String ID_LOGIN_FORM = "login";
    public static final HttpRequestMethods LOGIN_METHOD = HttpRequestMethods.POST;
    public static final String LOGIN_ACTION = "/login/index.php";

    public static final String FIELD_USER = "username";
    public static final String FIELD_PASS = "password";

    public static final String ID_ERROR = "loginerrormessage";

    public static final String CLASS_COURSE = "coursebox";

    public static final String CLASS_ACTIVITY = "activityinstance";
    public static final String CLASS_ACTIVITY_ICON = "activityicon";
    public static final String CLASS_ACTIVITY_NAME = "instancename";

    public static final String CLASS_BLIND = "accesshide";

    public static final String CLASS_TABS_SECTION = "single-section";
    public static final String CLASS_TABS = "tabtree";
    public static final String CLASS_THIS_TAB = "here";

    public static final String CLASS_FILE = "fp-filename-icon";
    public static final String CLASS_FILE_ICON = "fp-icon";
    public static final String CLASS_FILE_NAME = "fp-filename";
    
    public static final String CLASS_TOPIC = "discussion";
    public static final String CLASS_TOPIC_ICON = "picture";
    public static final String CLASS_TOPIC_NAME = "topic";

}
