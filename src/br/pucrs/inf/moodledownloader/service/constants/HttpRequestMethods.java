package br.pucrs.inf.moodledownloader.service.constants;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public enum HttpRequestMethods {

    GET, POST;

}
