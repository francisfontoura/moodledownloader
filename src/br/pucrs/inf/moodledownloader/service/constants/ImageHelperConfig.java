package br.pucrs.inf.moodledownloader.service.constants;

import br.pucrs.inf.moodledownloader.service.images.impl.ImageHelperSalamander;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public abstract class ImageHelperConfig {
    
    public static final Class IMAGE_HELPER = ImageHelperSalamander.class;
    
}
