package br.pucrs.inf.moodledownloader.service.http.builders;

import br.pucrs.inf.moodledownloader.service.constants.HttpRequestMethods;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class ApacheHttpRequestBuilder {

    private String action;
    private HttpRequestMethods method;
    private final Map<String, String> params;

    private static ApacheHttpRequestBuilder instance;

    private ApacheHttpRequestBuilder() {
        method = HttpRequestMethods.GET;
        params = new HashMap();
    }

    public static ApacheHttpRequestBuilder getInstance() {
        if (instance == null) {
            instance = new ApacheHttpRequestBuilder();
        }
        return instance;
    }

    protected void setAction(String url) {
        action = url;
    }

    protected void setMethod(HttpRequestMethods method) {
        this.method = method;
    }

    protected void addParam(String key, String value) {
        params.put(key, value);
    }

    private boolean isActionSet() {
        return (action != null);
    }

    private void putParamsInActionUrl() {
        if (params.size() > 0) {
            action += "?";
            for (Map.Entry<String, String> entry : params.entrySet()) {
                action += entry.getKey() + "=" + entry.getValue();
            }
        }
        System.out.println(action);
    }

    private UrlEncodedFormEntity getUrlEncodedFormEntity() throws Exception {
        List<NameValuePair> list = new ArrayList(params.size());
        for (Map.Entry<String, String> entry : params.entrySet()) {
            list.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        try {
            return new UrlEncodedFormEntity(list);
        } catch (UnsupportedEncodingException ex) {
            throw new Exception("Codificação não suportada.", ex);
        }
    }

    public HttpUriRequest build() throws Exception {
        if (!isActionSet()) {
            throw new Exception("URL de ação não definida.");
        }
        switch (method) {
            case GET:
                putParamsInActionUrl();
                params.clear();
                return new HttpGet(action);
            case POST:
                HttpPost post = new HttpPost(action);
                post.setEntity(getUrlEncodedFormEntity());
                params.clear();
                return post;
        }
        return null;
    }

}
