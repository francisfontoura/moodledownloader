package br.pucrs.inf.moodledownloader.service.http.builders;

import org.apache.http.client.CookieStore;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class ApacheHttpClientBuilder {

    private CookieStore cookieStore;
    private RequestConfig requestConfig;

    private static ApacheHttpClientBuilder instance;

    private ApacheHttpClientBuilder() {
        requestConfig = RequestConfig.DEFAULT;
    }

    public static ApacheHttpClientBuilder getInstance() {
        if (instance == null) {
            instance = new ApacheHttpClientBuilder();
        }
        return instance;
    }

    protected void useCookies() {
        if (cookieStore == null) {
            cookieStore = new BasicCookieStore();
        }
    }

    protected void setTimeout(int timeout) {
        requestConfig = RequestConfig.custom()
                .setSocketTimeout(timeout)
                .setConnectTimeout(timeout)
                .build();
    }

    public CloseableHttpClient build() {
        return HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .setDefaultRequestConfig(requestConfig)
                .build();
    }

}
