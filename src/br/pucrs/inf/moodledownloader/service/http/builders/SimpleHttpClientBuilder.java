package br.pucrs.inf.moodledownloader.service.http.builders;

import br.pucrs.inf.moodledownloader.service.constants.HttpRequestMethods;
import br.pucrs.inf.moodledownloader.service.http.SimpleHttpClient;

/**
 *
 * @author francis.fontoura@acad.pucrs.br
 */
public class SimpleHttpClientBuilder {

    public SimpleHttpClientBuilder() {
    }

    public SimpleHttpClient build() throws Exception {
        SimpleHttpClient client = SimpleHttpClient.getInstance();
        client.init();
        return client;
    }

    public SimpleHttpClientBuilder useCookies() {
        ApacheHttpClientBuilder.getInstance().useCookies();
        return this;
    }

    public SimpleHttpClientBuilder setTimeout(int timeout) {
        ApacheHttpClientBuilder.getInstance().setTimeout(timeout);
        return this;
    }

    public SimpleHttpClientBuilder setAction(String url) {
        ApacheHttpRequestBuilder.getInstance().setAction(url);
        return this;
    }

    public SimpleHttpClientBuilder setMethod(HttpRequestMethods method) {
        ApacheHttpRequestBuilder.getInstance().setMethod(method);
        return this;
    }

    public SimpleHttpClientBuilder addParam(String key, String value) {
        ApacheHttpRequestBuilder.getInstance().addParam(key, value);
        return this;
    }

}
