package br.pucrs.inf.moodledownloader.service.http;

import br.pucrs.inf.moodledownloader.service.http.builders.SimpleHttpClientBuilder;
import br.pucrs.inf.moodledownloader.service.http.builders.ApacheHttpRequestBuilder;
import br.pucrs.inf.moodledownloader.service.http.builders.ApacheHttpClientBuilder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;

/**
 * Interface mais simples para o Apache HttpComponents Client.
 *
 * @author francis.fontoura@acad.pucrs.br
 * @version 2014-08-08
 * @see <a href="http://hc.apache.org/">Apache HttpComponents</a>
 */
public class SimpleHttpClient {

    private CloseableHttpClient client;
    private HttpUriRequest request;
    private CloseableHttpResponse response;
    private HttpEntity entity;

    private static SimpleHttpClient instance;

    private SimpleHttpClient() {
    }

    public void init() throws Exception {
        client = ApacheHttpClientBuilder.getInstance().build();
        request = ApacheHttpRequestBuilder.getInstance().build();
        response = getResponse();
        entity = response.getEntity();
    }

    public static SimpleHttpClient getInstance() throws Exception {
        if (instance == null) {
            throw new Exception("Não construído.");
        }
        return instance;
    }

    private CloseableHttpResponse getResponse() throws Exception {
        if (response != null) {
            response.close();
        }
        try {
            return client.execute(request);
        } catch (IOException ex) {
            throw new Exception("Erro de E/S ao executar requisição.", ex);
        }
    }

    public static SimpleHttpClientBuilder custom() {
        instance = new SimpleHttpClient();
        return new SimpleHttpClientBuilder();
    }

    @Override
    public String toString() {
        try {
            StringBuilder sb = new StringBuilder();
            Scanner sc = new Scanner(entity.getContent());
            while (sc.hasNextLine()) {
                sb.append(sc.nextLine());
            }
            return sb.toString();
        } catch (IOException | IllegalStateException ex) {
            return ex.getMessage();
        }
    }

    public void toFile(File file) throws Exception {
        try {
            if ((file.getParentFile().exists() || file.getParentFile().mkdirs()) && (file.exists() || file.createNewFile())) {
                FileOutputStream out = new FileOutputStream(file);
                entity.writeTo(out);
            } else {
                throw new Exception("Não foi possível criar o arquivo: " + file.getName());
            }
        } catch (FileNotFoundException ex) {
            throw new Exception("Arquivo não encontrado.", ex);
        } catch (IOException ex) {
            throw new Exception("Erro de E/S ao salvar arquivo.", ex);
        }
    }

    public void close() throws Exception {
        try {
            response.close();
            client.close();
        } catch (IOException ex) {
            throw new Exception("Erro de E/S.", ex);
        }
    }

}
